from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from .views import user
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.

class UserProfileLabUnitTest(TestCase):

    def test_userprofile_url_is_exist(self):
        response = Client().get('/user/')
        self.assertEqual(response.status_code, 200)

    def test_userprofile_using_index_function(self):
        found = resolve('/user/')
        self.assertEqual(found.func, user)

class UserProfileFunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(UserProfileFunctionalTest, self).setUp()

    # def test_title(self):
    #     selenium = self.selenium
    #     selenium.get(self.live_server_url)
    #     time.sleep(3)
    #     self.assertIn("Profile Time!", selenium.title)

    def test_body_text_with_css_property(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        body_style = selenium.find_element_by_tag_name('body').value_of_css_property('font-family')
        time.sleep(3)
        self.assertIn('Muli', body_style)

    def tearDown(self):
        time.sleep(3)
        self.selenium.quit()
        super(UserProfileFunctionalTest, self).tearDown()
