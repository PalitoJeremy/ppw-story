$(document).ready(function () {
    $("#accordion").accordion({
        collapsible: true,
        active: false,
        heightStyle: "content",
    });

    $('textarea').autoResize();

    var area = document.getElementById("content-text");
    var counter = document.getElementById("counter");
    var maxLength = 300;
    var checkLength = function () {
        if (area.value.length <= maxLength) {
            counter.innerHTML = (maxLength - area.value.length) + " characters remaining";
        }
    };
    setInterval(checkLength, 50);

    var local = window.localStorage;

    if(local.getItem('isDark') === 'true') {
      $('#body').removeClass('body')
      $('#body').addClass('body-dark')
      $('#change-theme').removeClass('btn btn-light')
      $('#change-theme').addClass('btn btn-dark')
    }
    else {
      $('#body').removeClass('body-dark')
      $('#body').addClass('body')
      $('#change-theme').removeClass('btn btn-dark')
      $('#change-theme').addClass('btn btn-light')
    }

    document.getElementById('change-theme').onclick = ubahTema;

    function ubahTema() {
      if ($("body").hasClass('body')) {
        $('#body').removeClass('body')
        $('#body').addClass('body-dark')
        $('#change-theme').removeClass('btn btn-light')
        $('#change-theme').addClass('btn btn-dark')
        local.setItem('isDark', true);
      }
      else {
        $('#body').removeClass('body-dark')
        $('#body').addClass('body')
        $('#change-theme').removeClass('btn btn-dark')
        $('#change-theme').addClass('btn btn-light')
        local.setItem('isDark', false);
      }
    }

})
