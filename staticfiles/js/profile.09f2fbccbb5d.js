var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 3000);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}

$( function() {
  $( "#accordion" ).accordion({active: false, collapsible: true});
  } );

function ubahTema() {
  var bodyelement = document.getElementById('body')
  if (bodyelement.className == 'body') {
    document.getElementById('body').className = 'body-dark';
    document.getElementById('buttonTheme').className = 'btn btn-outline-light';
    document.getElementByTagName('pre').className = 'darktext';
  }
  else {
    document.getElementById('body').className = 'body';
    document.getElementById('buttonTheme').className = 'btn btn-outline-dark';
    document.getElementByTagName('pre').className = 'lighttext';
  }
}
