from django.urls import path
from .views import sub, validateEmail, saveToModel

urlpatterns = [
    path('', sub, name="subscribe"),
    path('validate/', validateEmail, name="validate"),
    path('post/', saveToModel, name="post"),
]
