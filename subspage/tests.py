from django.db import IntegrityError
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import sub
from .models import SubsData
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.

class SubsPageUnitTest(TestCase):
    def test_url_subspage_is_exist(self):
        response = Client().get('/subscribe/')
        self.assertEqual(response.status_code, 200)

    def test_url_subscribe_using_subscribe_function(self):
        found = resolve('/subscribe/')
        self.assertEqual(found.func, sub)

    def test_model_can_save_data(self):
        SubsData.objects.create(name="Tester", email='test@ui.ac.id', password="asdasd123")

        counting_all_data = SubsData.objects.all().count()
        self.assertEqual(counting_all_data, 1)

    def test_email_is_unique(self):
        SubsData.objects.create(email="test@email.com")
        with self.assertRaises(IntegrityError):
            SubsData.objects.create(email="test@email.com")

    def test_post_using_ajax(self):
        response = Client().post('/subscribe/post/', data={
            "name": "yuhu",
            "email": "yuhu@gmail.com",
            "password": "123",
        })
        self.assertEqual(response.status_code, 200)

    def test_check_email_already_exist_view_get_return_200(self):
        SubsData.objects.create(name="yuhu",
                                      email="yuhu@gmail.com",
                                      password="123")
        response = Client().post('/subscribe/validate/', data={
            "email": "yuhu@gmail.com"
        })
        self.assertEqual(response.json()['is_exists'], True)

class SubsPageFunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(SubsPageFunctionalTest, self).setUp()

    def test_body_text_with_css_property(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        body_style = selenium.find_element_by_tag_name('body').value_of_css_property('font-family')
        time.sleep(3)
        self.assertIn('Muli', body_style)

    def tearDown(self):
        time.sleep(3)
        self.selenium.quit()
        super(SubsPageFunctionalTest, self).tearDown()
