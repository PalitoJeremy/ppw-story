from django import forms
from .models import SubsData


class SubsForm(forms.ModelForm):
    class Meta:
        model = SubsData
        fields = '__all__'

    name = forms.CharField(label="Full Name", max_length=100,
                           widget=forms.TextInput(
                               attrs={'id': 'name', 'class': 'form-control', 'placeholder': 'Enter your full name'}))
    email = forms.EmailField(label="Email", max_length=100,
                             widget=forms.EmailInput(attrs={'id': 'email', 'class': 'form-control',
                                                            'placeholder': 'Enter your valid email'}))
    password = forms.CharField(label='Password', max_length=30, widget=forms.PasswordInput(
        attrs={'id': 'password', 'class': 'form-control', 'placeholder': 'Enter your password'}))
