from django.http import JsonResponse
from django.shortcuts import render
from .forms import SubsForm
from .models import SubsData

# Create your views here.

def sub(request):
    return render(request, 'subscribe.html', {'form': SubsForm})

def validateEmail(request):
    if request.method == 'POST':
        email = request.POST['email']
        check_email = SubsData.objects.filter(email=email)
        if check_email.exists():
            return JsonResponse({'is_exists': True})
        return JsonResponse({'is_exists': False})

def saveToModel(request):
    if request.method == 'POST':
        name = request.POST['name']
        email = request.POST['email']
        password = request.POST['password']
        SubsData.objects.create(name=name, email=email, password=password)
        return JsonResponse({'is_success': True})
